# Strategy23AMunoz - Alejandro Muñoz

## Risketos Basics:

- [X] Capacitat d'eleccio, per exemple diferents torretes.
- [X] El HUD es crea i s'emplena per BP.
- [ ] Us d'esdeveniments i dispatchers, tot ha d'estar desacoplat.
- [X] Enemics o objectius a on atacar i en fer-ho que morin.
- [X] Us d'un actor component o Interficie.

## Risketos Opcionals:

- [ ] Fer una pool d'objectes.
- [X] Augment progressiu de dificultat. (Diferents tipus d'enemics amb diferent HP i hi ha un tipus concret que ataquen a les torres)
- [ ] Mapa de joc generat de manera procedimental.
- [X] Menu inicial amb diversos nivells.
- [X] 'Waves' personalitzables a on posar quantitat d'enemics i tipus d'enemics a apareixer.
- [X] Preview de la torreta

## Controls

- Left Click -> Open / Close menu, select turret
- Right Click -> Deploy turret

Per testing millor jugar al nivell 2